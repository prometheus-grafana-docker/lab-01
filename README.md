# Prometheus and Grafana with Docker
# Lab 01: Create Prometheus and Grafana configuration files

---

# Preparations
 - We will install prometheus and grafana on 2 separate containers using docker and docker-compose

 - We will demonstrate metricsses coming from prometheus container
  
 - Get monitoring server IP (ask the instructor), OR work on your local PC
 
 - Credentials: 
 username:**sela** , password:**sela**

---

# Tasks

 - Make sure docker, docker-compose and curl are installed
 
 - Create persistent volume
 
 - Create  prometheus configuration file

 - Create  grafana configuration and datasource files

 - Create  docker-compose configuration file


--- 
 
## Make sure docker, docker-compose and curl are installed

&nbsp;

 - Inside the server open bash (git bash on windows) and run commands:

```
docker version
docker-compose version
curl

```
<img alt="Image 1.1" src="images/verification.png" width="75%" height="75%">

&nbsp;

 - Create persistent volume, run command:
 
```
mkdir -p ~/prometheus-grafana/{grafana,prometheus}

```

&nbsp;

 - Create  prometheus configuration file
 - Please note that the metrics to be exposed are for the prometheus server itself (localhost:9090)
 - run command:

```

tee ~/prometheus-grafana/prometheus/prometheus.yml<<EOF
global:
  scrape_interval: 15s
  scrape_timeout: 10s
  evaluation_interval: 15s
alerting:
  alertmanagers:
  - static_configs:
    - targets: []
    scheme: http
    timeout: 10s
    api_version: v1
scrape_configs:
- job_name: prometheus
  honor_timestamps: true
  scrape_interval: 15s
  scrape_timeout: 10s
  metrics_path: /metrics
  scheme: http
  static_configs:
  - targets:
    - localhost:9090
    - node-exporter:9100
EOF

```

&nbsp;

- Create  grafana configuration file, run command:

```
  curl  https://raw.githubusercontent.com/grafana/grafana/v7.5.1/conf/defaults.ini -o ~/prometheus-grafana/grafana/grafana.ini

```

&nbsp;


- Create  grafana configuration datasource file, run command:

```

tee ~/prometheus-grafana/grafana/datasource.yml<<EOF
apiVersion: 1
datasources:
- name: Prometheus
  type: prometheus
  url: http://localhost:9090 
  isDefault: true
  access: direct
  editable: true
EOF

```
